use std::fs;
use std::str::{Lines};

fn to_integer(number: &str) -> i32 {
    match number {
        "1" | "one" => 1,
        "2" | "two" => 2,
        "3" | "three" => 3,
        "4" | "four" => 4,
        "5" | "five" => 5,
        "6" | "six" => 6,
        "7" | "seven" => 7,
        "8" | "eight" => 8,
        "9" | "nine" => 9,
        _ => panic!()
    }
}

fn calc(content: Lines, numbers: Vec<&str>) {
    let mut res = 0;

    for line in content {
        let found = numbers.iter().flat_map(|number| line.match_indices(number)).collect::<Vec<_>>();
        // println!("{:?}", found);
        let (_, first) = found.iter().min().unwrap();
        let (_, last) = found.iter().max().unwrap();

        res += to_integer(first) * 10 + to_integer(last);
    }
    println!("Sum of all calibration values: {}", res);
}

fn main() {
    let content = fs::read_to_string("inputs.txt").unwrap();
    let part1 = vec!["1", "2", "3", "4", "5", "6", "7", "8", "9"];
    let part2 = vec!["1", "2", "3", "4", "5", "6", "7", "8", "9",
                     "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"];

    calc(content.lines(), part1);
    calc(content.lines(), part2);
}
