use std::collections::HashMap;
use std::fs;

fn main() {
    let content = fs::read_to_string("inputs.txt").unwrap();
    let colors = HashMap::from([
        ("red", 12),
        ("green", 13),
        ("blue", 14),
    ]);
    let mut part_1 = 0;
    let mut part_2 = 0;

    for line in content.lines() {
        let infos:  Vec<&str> = line.split(':').collect();

        let game_number = infos[0][5..].parse::<i32>().unwrap();

        let rolls= infos[1].split([';', ',']);
        let rolls = rolls.map(|roll| roll.trim().split_whitespace().collect::<Vec<&str>>());

        if rolls.clone().all(|roll| roll[0].parse::<i32>().unwrap() <= colors[roll[1]]) {
            part_1 += game_number
        }

        let mut max_colors = HashMap::from([
            ("red", 0),
            ("green", 0),
            ("blue", 0),
        ]);
        for roll in rolls {
            let value = roll[0].parse::<i32>().unwrap();
            if value > max_colors[roll[1]] {
                max_colors.insert(roll[1], value);
            }
        }
        part_2 += max_colors["blue"] * max_colors["red"] * max_colors["green"];
    }
    println!("Sum of valid games: {}", part_1);
    println!("Sum of minimum ball needed: {}", part_2);
}
