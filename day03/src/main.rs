use std::fs;

fn get_square_tiles(map: &Vec<Vec<char>>, x: usize, y: usize, size: usize) -> Vec<char>
{
    let mut tiles = Vec::new();
    let map_height = map.len();
    let map_width = map[0].len();

    let start_x = x.saturating_sub(1);
    let end_x = (x + size).min(map_width - 1);
    let start_y = y.saturating_sub(1);
    let end_y = (y + 1).min(map_height - 1);

    for j in start_y..=end_y {
        for i in start_x..=end_x {
            tiles.push(map[j][i]);
        }
    }
    tiles
}

fn str_to_number(map: &Vec<Vec<char>>, y: usize, start: usize, end: usize) -> i32 {
    map[y][start..end].iter().collect::<String>().parse::<i32>().unwrap()
}

fn find_number(map: &Vec<Vec<char>>, x: usize, y: usize) -> i32 {
    let mut start_pos = x;
    let mut end_pos = x;

    while start_pos > 0 && map[y][start_pos - 1].is_numeric() {
        start_pos -= 1;
    }
    while map[y][end_pos].is_numeric() {
        end_pos += 1;
    }
    str_to_number(map, y, start_pos, end_pos)
}

fn find_number_position(tiles: Vec<char>) -> Vec<(usize, usize)> {
    let mut positions = Vec::new();
    let mut k = 0;
    while k < 3 {
        let mut l = 0;
        while l < 3 {
            if tiles[k * 3 + l].is_numeric() {
                positions.push((k, l));
                while l < 3 && tiles[k * 3 + l].is_numeric() {
                    l += 1;
                }
            } else {
                l += 1;
            }
        }
        k += 1;
    }
    positions
}

fn handle_part2(map: &Vec<Vec<char>>, i: usize, j: usize) -> i32 {
    let tiles = get_square_tiles(map, j, i, 1);
    let positions = find_number_position(tiles);

    if positions.len() == 2 {
        let (y2, x2) = positions[0];
        let first_number = find_number(map, (j + x2) - 1, (i + y2) - 1);
        let (y2, x2) = positions[1];
        let second_number = find_number(map, (j + x2) - 1, (i + y2) - 1);
        first_number * second_number
    } else {
        0
    }
}

fn main() {
    let content = fs::read_to_string("inputs.txt").unwrap();
    let schematic: Vec<Vec<_>> = content.split('\n').map(|s| s.chars().collect::<Vec<_>>()).collect();

    let mut result_part1 = 0;
    let mut result_part2 = 0;
    let mut i = 0;

    while i < schematic.len() {
        let mut j = 0;
        while j < schematic[i].len() {
            if schematic[i][j].is_numeric() {
                let start_pos = j;
                while j < schematic[i].len() && schematic[i][j].is_numeric() {
                    j += 1;
                }
                let tiles = get_square_tiles(&schematic, start_pos, i, j - start_pos);
                if !tiles.iter().all(|c| c.is_numeric() || *c == '.') {
                    result_part1 += str_to_number(&schematic, i, start_pos, j);
                }
            } else if schematic[i][j] == '*' {
                result_part2 += handle_part2(&schematic, i, j);
                j += 1;
            }
            else {
                j += 1;
            }
        }
        i += 1;
    }
    println!("Sum of all schematic parts: {}", result_part1);
    println!("Sum of all gear parts ratio: {}", result_part2);
}
