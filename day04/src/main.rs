use std::fs;
use std::str::Lines;

#[derive(Debug)]
struct Ticket {
    number_of_ticket: u32,
    winnings: usize
}

impl Ticket {
    fn increase_number_of_tickets(&mut self, nb: u32) {
        self.number_of_ticket += nb;
    }
}

fn parse_numbers(numbers: &str) -> Vec<usize> {
    numbers.split_whitespace().map(|x| x.parse::<usize>().unwrap()).collect::<Vec<usize>>()
}

fn parse_tickets(lines: Lines) -> Vec<Ticket> {
    let mut tickets: Vec<Ticket> = Vec::new();

    for line in lines {
        let components = line.split([':', '|']).collect::<Vec<&str>>();

        let winning = parse_numbers(components[1]);
        let mine = parse_numbers(components[2]);

        let count_winning = mine.iter().filter(|nb| winning.contains(nb)).count();

        tickets.push(Ticket {
            number_of_ticket: 1,
            winnings: count_winning
        })
    }
    tickets
}

fn calculate_pow_winnings(ticket: &Ticket) -> u32 {
    if ticket.winnings > 0 {
        2_u32.pow((ticket.winnings - 1) as u32)
    } else {
        0
    }
}

fn main() {
    let content = fs::read_to_string("inputs.txt").unwrap();
    let tickets = content.lines();

    let mut tickets = parse_tickets(tickets);
    let res_part1 = tickets.iter().fold(0, |acc, ticket| acc + calculate_pow_winnings(ticket));
    println!("Total value of my gains: {res_part1}");

    for i in 0.. tickets.len() {
        let cur_number_of_tickets = tickets[i].number_of_ticket;

        for j in 1..=tickets[i].winnings {
            let tmp = &mut tickets[i + j];
            tmp.increase_number_of_tickets(cur_number_of_tickets);
        }
    }

    let res_part2 = tickets.iter().fold(0, |acc, ticket| acc + ticket.number_of_ticket);
    println!("Total number of my tickets: {res_part2}");

}
