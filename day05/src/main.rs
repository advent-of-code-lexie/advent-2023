use std::fs;
use std::cmp::min;

#[derive(Debug)]
struct Link {
    destination: isize,
    source: isize,
    range: isize
}

#[derive(Debug)]
struct Links {
    links: Vec<Link>,
}

fn parse_source_link(s: &str) -> Link {
    let values: Vec<isize> = s.split_whitespace().map(|elem| elem.parse::<isize>().unwrap()).collect();

    Link {
        destination: values[0],
        source: values[1],
        range: values[2]
    }
}

fn parse_source_map(map: &str) -> Links {
    let map: Vec<&str> = map.lines().collect();

    Links {
        links: map[1..].iter().map(|link| parse_source_link(link)).collect()
    }
}

fn build_maps(sections: &[&str]) -> Vec<Links> {
    sections.iter().map(|section| parse_source_map(section)).collect()
}

fn build_seeds(seeds: &str) -> Vec<isize> {
    let seeds = &seeds.split_whitespace().collect::<Vec<&str>>()[1..];
    seeds.iter().map(|seed| seed.parse().unwrap()).collect()
}

fn build_seeds_part1(seeds: &Vec<isize>) -> Vec<(isize, isize)> {
    seeds.iter().map(|seed| (*seed, 1_isize)).collect()
}

fn build_seeds_part2(seeds: &Vec<isize>) -> Vec<(isize, isize)> {
    (0..seeds.len()).step_by(2).map(|index| (seeds[index], seeds[index + 1])).collect()
}

fn find_link_by_range(start: isize, length: isize, map: &Links) -> Vec<(isize, isize)> {
    let mut length = length;
    let mut start = start;
    let mut links = Vec::new();

    while length > 0 {
        let link = map.links.iter().filter(|link| {
            let delta = start - link.source;
            (0..link.range).contains(&delta)
        }).next();

        match link {
            Some(link) => {
                let delta = start - link.source;
                let len = min(link.range - delta, length);
                links.push((link.destination + delta, len));
                length -= len;
                start += len;
            },
            None => {
                links.push((start, length));
                break
            }
        }
    }
    links
}

fn find_lowest_location(seeds: Vec<(isize, isize)>, maps: &Vec<Links>) -> isize {
    let mut seeds = seeds;

    for map in maps {
        seeds = seeds.iter().flat_map(|(seed, len)| find_link_by_range(*seed, *len, map)).collect();
    }

    seeds.iter().min_by_key(|(seed, _)| seed).unwrap().0
}

fn main() {
    let content = fs::read_to_string("inputs.txt").unwrap();
    // let content = fs::read_to_string("inputs_example.txt").unwrap();

    let sections = content.split("\n\n").collect::<Vec<&str>>();

    let seeds = build_seeds(sections[0]);
    let seeds_part1 = build_seeds_part1(&seeds);
    let seeds_part2 = build_seeds_part2(&seeds);

    let maps= build_maps(&sections[1..]);

    let part_1 = find_lowest_location(seeds_part1, &maps);
    println!("Result of part 1: {}", part_1);

    let part_2 = find_lowest_location(seeds_part2, &maps);
    println!("Result of part 2: {}", part_2);
}
