use std::collections::HashMap;
use std::{fs, thread};
use std::sync::Arc;

#[derive(Debug)]
struct SourceLink {
    destination: isize,
    source: isize,
    range: isize
}

#[derive(Debug)]
struct SourceMap {
    links: Vec<SourceLink>,
    src_name: String,
    dest_name: String
}

fn get_source_destination(s: &str) -> (&str, &str) {
    let values: Vec<&str> = s.split(['-', ' ']).collect();

    (values[0], values[2])
}

fn parse_source_link(s: &str) -> SourceLink {
    let values: Vec<isize> = s.split_whitespace().map(|elem| elem.parse::<isize>().unwrap()).collect();

    SourceLink {
        destination: values[0],
        source: values[1],
        range: values[2]
    }
}

fn parse_source_map(map: &str) -> SourceMap {
    let map: Vec<&str> = map.lines().collect();
    let mut links = Vec::new();

    let (src_name, dest_name) = get_source_destination(map[0]);
    let src_name = String::from(src_name);
    let dest_name = String::from(dest_name);

    for link in &map[1..] {
        links.push(parse_source_link(link))
    }

    let links: Vec<SourceLink> = map[1..].iter().map(|link| parse_source_link(link)).collect();

    SourceMap {
        links,
        src_name,
        dest_name
    }
}

fn find_location(nb: isize, source: &SourceMap, maps: &HashMap<String, SourceMap>) -> isize {
    let found_link = source.links.iter().find(|link| {
        nb >= link.source && nb < link.source + link.range
    });

    let diff = match found_link {
        Some(link) => link.destination - link.source,
        None => 0
    };
    if source.dest_name == "location" {
        nb + diff
    } else {
        find_location(nb + diff, &maps[&source.dest_name], maps)
    }
}

fn build_maps(sections: &[&str]) -> HashMap<String, SourceMap> {
    let mut maps = HashMap::new();

    for section in sections {
        let map = parse_source_map(section);
        maps.insert(map.src_name.clone(), map);
    }

    maps
}

fn build_seeds(seeds: &str) -> Vec<isize> {
    let seeds = &seeds.split_whitespace().collect::<Vec<&str>>()[1..];
    seeds.iter().map(|seed| seed.parse().unwrap()).collect()
}

fn build_thread_seeds(seeds: &Vec<isize>) -> Arc<Vec<isize>> {
    let mut thread_seeds = Vec::new();

    for i in (0..seeds.len()).step_by(2) {
        let start_seed = seeds[i];
        let bound = seeds[i] + seeds[i + 1];

        for seed in start_seed..bound {
            thread_seeds.push(seed);
        }
    }

    Arc::new(thread_seeds)
}

fn main() {
    let content = fs::read_to_string("inputs.txt").unwrap();
    // let content = fs::read_to_string("test_inputs.txt").unwrap();

    let sections = content.split("\n\n").collect::<Vec<&str>>();

    let seeds = build_seeds(sections[0]);
    let thread_seeds = build_thread_seeds(&seeds);
    let maps= build_maps(&sections[1..]);

    let location = seeds.iter().map(|seed| {
        find_location(*seed, &maps["seed"], &maps)
    }).min().unwrap();
    println!("Result of part 1: {:?}", location);

    let nb_seeds = thread_seeds.len();
    let nb_threads = 12;
    let chunk_size = nb_seeds / nb_threads;

    let mut handles = Vec::new();
    let maps = Arc::new(maps);

    for i in 0..=nb_threads {
        let start_index = i * chunk_size;
        let bound = if start_index + chunk_size > nb_seeds { nb_seeds } else { start_index + chunk_size };

        let maps_clone = Arc::clone(&maps);
        let thread_seeds_clone = Arc::clone(&thread_seeds);

        let handle = thread::spawn(move || {
            println!("Processing original seed : {}", thread_seeds_clone[start_index]);
            thread_seeds_clone[start_index..bound].iter().map(|seed| {
                find_location(*seed, &maps_clone["seed"], &maps_clone)
            }).min().unwrap().clone()
        });
        handles.push(handle);
    }

    let mut location = Vec::new();
    for handle in handles {
        let result = handle.join().unwrap();
        location.push(result);
    }

    let min_location = location.iter().min().unwrap();
    println!("Result of part 2: {:?}", min_location);
}
