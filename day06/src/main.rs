use std::fs;

fn parse_line(line: &str) -> Vec<usize> {
    line.split_whitespace().collect::<Vec<&str>>()[1..]
        .iter().map(|time| time.parse().unwrap()).collect()
}

fn parse_line_part2(line: &str) -> usize {
    line.split_whitespace().collect::<Vec<&str>>()[1..].join("").parse().unwrap()
}

fn calculate_possibilities(time: usize, record: usize) -> usize {
    let mut possibilities = 0;
    for j in 1..time {
        if (time - j) * j > record {
            possibilities += 1;
        }
    }
    possibilities
}

fn main() {
    let content = fs::read_to_string("inputs.txt").unwrap();
    // let content = fs::read_to_string("inputs_example.txt").unwrap();

    let lines: Vec<&str> = content.lines().collect();

    let times = parse_line(&lines[0]);
    let records = parse_line(&lines[1]);
    let mut result = 1;
    for i in 0..times.len() {
        result *= calculate_possibilities(times[i], records[i]);
    }
    println!("Result of part 1: {result}");

    let time = parse_line_part2(&lines[0]);
    let record = parse_line_part2(&lines[1]);

    let possibilities = calculate_possibilities(time, record);
    println!("Result of part 2: {possibilities}");
}
