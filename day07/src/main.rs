use std::cmp::Ordering;
use std::cmp::Ordering::{Equal};
use std::collections::HashMap;
use std::fs;
use std::iter::zip;
use std::str::Lines;
use crate::HandType::NONE;

#[derive(Debug)]
struct Hand {
    cards: String,
    value: usize,
    hand_type: HandType,
    letter_count: HashMap<char, i32>
}

#[derive(Debug, Ord, PartialOrd, Eq, PartialEq, Copy, Clone)]
enum HandType {
    FIVE = 1,
    FOUR = 2,
    FULL = 3,
    THREE = 4,
    TWOPAIR = 5,
    PAIR = 6,
    HIGH = 7,
    NONE = 8
}

fn find_hand_type(values: &Vec<i32>) -> HandType {
    match values[..] {
        [5] => HandType::FIVE,
        [1, 4] => HandType::FOUR,
        [2, 3] => HandType::FULL,
        [1, 1, 3] => HandType::THREE,
        [1, 2, 2] => HandType::TWOPAIR,
        [1, 1, 1, 2] => HandType::PAIR,
        [1, 1, 1, 1, 1] => HandType::HIGH,
        _ => NONE
    }
}

fn compare_hands(a: &Hand, b: &Hand, card_values: &HashMap<char, isize>) -> Ordering {
    let diff = a.hand_type.cmp(&b.hand_type);
    if diff != Equal {
        return diff
    }
    for (c1, c2) in zip(a.cards.chars(), b.cards.chars()) {
        if c1 != c2 {
            return card_values[&c1].cmp(&card_values[&c2]);
        }
    }
    Equal
}

fn build_hands(lines: Lines) -> Vec<Hand> {
    lines.map(|line| {
        let line: Vec<&str> = line.split_whitespace().collect();
        let cards = String::from(line[0]);
        let letter_count = get_letter_count(&cards);
        Hand {
            cards,
            value: line[1].parse::<usize>().unwrap(),
            hand_type: NONE,
            letter_count
        }
    }).collect()
}

fn get_letter_count(cards: &String) -> HashMap<char, i32> {
    let mut letter_counts = HashMap::new();

    for c in cards.chars() {
        *letter_counts.entry(c).or_insert(0) += 1;
    }

    letter_counts
}

fn order_and_calculation(hands: &mut Vec<Hand>, joker: bool) {
    let card_values: HashMap<char, isize> = if joker {
        HashMap::from([
            ('A', 1), ('K', 2), ('Q', 3), ('T', 4),
            ('9', 5), ('8', 6), ('7', 7), ('6', 8), ('5', 9), ('4', 10), ('3', 11), ('2', 12), ('J', 13)
        ])
    } else {
        HashMap::from([
            ('A', 1), ('K', 2), ('Q', 3), ('J', 4), ('T', 5),
            ('9', 6), ('8', 7), ('7', 8), ('6', 9), ('5', 10), ('4', 11), ('3', 12), ('2', 13)
        ])
    };

    hands.sort_by(|a, b| compare_hands(a, b, &card_values));
    hands.reverse();

    let result = hands.iter().enumerate()
        .fold(0, |acc, (index, hand)| acc + hand.value * (index + 1));
    println!("Result with joker {joker}: {result}");
}

fn main() {
    let content = fs::read_to_string("inputs.txt").unwrap();
    // let content = fs::read_to_string("inputs_example.txt").unwrap();

    let lines = content.lines();
    let mut hands = build_hands(lines);

    // part 1
    for hand in &mut hands {
        let mut letters: Vec<i32> = hand.letter_count.clone().into_values().collect();
        letters.sort();
        hand.hand_type = find_hand_type(&letters);
    }
    order_and_calculation(&mut hands, false);

    // part 2
    for hand in &mut hands {
        const J: char = 'J';
        let nb_j = if hand.letter_count.contains_key(&J) && hand.letter_count[&J] < 5 {
            hand.letter_count.remove_entry(&J).unwrap().1
        } else {
            0
        };

        let mut letters: Vec<i32> = hand.letter_count.clone().into_values().collect();
        letters.sort();
        *letters.last_mut().unwrap() += nb_j;
        hand.hand_type = find_hand_type(&letters);
    }
    order_and_calculation(&mut hands, true);
}
