use std::collections::HashMap;
use std::fs;

#[derive(Debug)]
struct Instruction {
    value: String,
    left: String,
    right: String
}

fn parse_map(map: &str) -> Instruction {
    Instruction {
        value: String::from(&map[..=2]),
        left: String::from(&map[7..=9]),
        right: String::from(&map[12..=14])
    }
}

fn get_maps(maps: &[&str]) -> HashMap<String, Instruction> {
    maps.iter().map(|map| {
        let instruction = parse_map(map);
        (instruction.value.clone(), instruction)
    }).collect()
}

fn find_my_path(maps: &HashMap<String, Instruction>, instructions: &str) -> usize {
    let mut map = &maps["AAA"];
    let mut steps: usize = 0;
    loop {
        for instruction in instructions.chars() {
            if map.value == "ZZZ" {
                return steps
            }
            if instruction == 'L' {
                map = &maps[&map.left];
            } else {
                map = &maps[&map.right];
            }
            steps += 1;
        }
    }
}

fn is_ghost_start(value: &String) -> bool {
    match value.chars().collect::<Vec<char>>()[..] {
        [_, _, 'A'] => true,
        _ => false
    }
}

fn is_ghost_end(value: &String) -> bool {
    match value.chars().collect::<Vec<char>>()[..] {
        [_, _, 'Z'] => true,
        _ => false
    }
}

fn find_ghost_end(map: &Instruction, maps:&HashMap<String, Instruction>, instructions: &str) -> usize {
    let mut steps: usize = 0;
    let mut map = map;
    loop {
        for instruction in instructions.chars() {
            if is_ghost_end(&map.value) {
                return steps
            }
            if instruction == 'L' {
                map = &maps[&map.left];
            } else {
                map = &maps[&map.right];
            }
            steps += 1;
        }
    }
}

// Greater Common Divisor using the Euclidean Algorithm
fn find_gcd(a: usize, b: usize) -> usize {
    if a == 0 { return b; }
    else if b == 0 { return a; }

    if a > b {
        find_gcd(b, a % b)
    } else {
        find_gcd(a, b % a)
    }
}

// Least Common Multiple using GCD
fn lcm(a: usize, b: usize) -> usize {
    a * (b / find_gcd(a, b))
}

fn find_ghost_path(maps: &HashMap<String, Instruction>, instructions: &str) -> usize {
    maps.values().filter(|map| is_ghost_start(&map.value))
        .map(|map| find_ghost_end(map, maps, instructions))
        .reduce(|a, b| lcm(a, b)).unwrap()
}

fn main() {
    let content = fs::read_to_string("inputs.txt").unwrap();
    // let content = fs::read_to_string("inputs_example.txt").unwrap();
    // let content = fs::read_to_string("inputs_example_part2.txt").unwrap();

    let lines: Vec<&str> = content.lines().collect();

    let instructions = lines[0];
    let maps = get_maps(&lines[2..]);

    println!("Result of part 1: {}", find_my_path(&maps, instructions));
    println!("Result of part 2: {}", find_ghost_path(&maps, instructions));
}
