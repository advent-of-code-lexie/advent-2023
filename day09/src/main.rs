use std::fs;

fn calculate_difference(input: &Vec<isize>) -> Vec<isize> {
    let mut res = Vec::new();
    let mut t = input.iter();
    let mut f = *t.next().unwrap();

    for value in t {
        res.push(value - f);
        f = *value;
    }

    res
}

fn calculate_part1(mut input: Vec<isize>) -> Vec<isize> {
    if input.iter().all(|x| *x == 0) {
        return input
    }

    let difference = calculate_difference(&input);
    let ret = calculate_part1(difference);

    input.push(input.last().unwrap() + ret.last().unwrap());
    input
}

fn calculate_part2(mut input: Vec<isize>) -> Vec<isize> {
    if input.iter().all(|x| *x == 0) {
        return input
    }

    let difference = calculate_difference(&input);
    let ret = calculate_part2(difference);

    input.insert(0, input.first().unwrap() - ret.first().unwrap());
    input
}

fn main() {
    let content = fs::read_to_string("inputs.txt").unwrap();
    // let content = fs::read_to_string("inputs_example.txt").unwrap();

    let inputs: Vec<Vec<isize>> = content.lines()
        .map(|line| {
            line.split_whitespace().map(|value| value.parse().unwrap()).collect()
        }).collect();

    let part_1 = inputs.iter().fold(0, |acc, input| acc + calculate_part1(input.clone()).last().unwrap());
    println!("Result of part 1: {part_1}");

    let part_2 = inputs.iter().fold(0, |acc, input| acc + calculate_part2(input.clone()).first().unwrap());
    println!("Result of part 2: {part_2}");
}
