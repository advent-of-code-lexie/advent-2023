use std::fs;
use crate::Orientation::{EAST, NONE, NORTH, SOUTH, WEST};

fn find_starting_position(map: &Vec<Vec<char>>) -> (usize, usize) {
    for (x, line) in map.iter().enumerate() {
        return match line.iter().position(|c| *c == 'S') {
            Some(y) => (x, y),
            None => continue
        };
    }
    (0, 0)
}

#[derive(PartialEq, Debug)]
enum Orientation {
    EAST,
    WEST,
    NORTH,
    SOUTH,
    NONE
}

fn valid_north_pipe(c: char) -> bool {
    match c {
        '|' | '7' | 'F' => true,
        _ => false
    }
}

fn valid_east_pipe(c: char) -> bool {
    match c {
        '-' | 'J' | '7' => true,
        _ => false
    }
}

fn valid_west_pipe(c: char) -> bool {
    match c {
        '-' | 'L' | 'F' => true,
        _ => false
    }
}

fn valid_south_pipe(c: char) -> bool {
    match c {
        '|' | 'L' | 'J' => true,
        _ => false
    }
}

fn find_next_pipe(map: &Vec<Vec<char>>, x: usize, y: usize, prev: Orientation) -> (usize, usize, Orientation) {
    let map_width = map[0].len();
    let map_height = map.len();

    if x > 0 && prev != NORTH && valid_north_pipe(map[x - 1][y]) {
        (x - 1, y, SOUTH)
    } else if y < map_width - 1 && prev != EAST && valid_east_pipe(map[x][y + 1]) {
        (x, y + 1, WEST)
    } else if x < map_height - 1 && prev != SOUTH && valid_south_pipe(map[x + 1][y]) {
        (x + 1, y, NORTH)
    } else if y > 0 && prev != WEST && valid_west_pipe(map[x][y - 1]) {
        (x, y - 1, EAST)
    } else {
        (0, 0, NONE)
    }
}

fn next_position(c: char, mut x: usize, mut y: usize, mut prev: Orientation) -> (usize, usize, Orientation) {
    match c {
        '|' => if prev == NORTH { x += 1 } else { x -= 1; prev = SOUTH },
        'L' => if prev == NORTH { y += 1; prev = WEST } else { x -= 1; prev = SOUTH },
        'J' => if prev == NORTH { y -=1; prev = EAST } else { x -= 1; prev = SOUTH },
        'F' => if prev == SOUTH { y += 1; prev = WEST } else { x += 1; prev = NORTH },
        '7' => if prev == SOUTH { y -= 1; prev = EAST } else { x += 1; prev = NORTH },
        '-' => if prev == EAST { y -= 1 } else { y += 1; prev = WEST }
        _ => { x = 0; y = 0; prev = NONE }
    }
    (x, y, prev)
}


fn get_path_positions(map: &Vec<Vec<char>>, x: usize, y: usize, prev: Orientation) -> Vec<(usize, usize)> {
    let (x, y, prev) = next_position(map[x][y], x, y, prev);

    if map[x][y] == 'S' {
        vec![(x, y)]
    } else {
        let mut res = get_path_positions(map, x, y, prev);
        res.push((x, y));
        res
    }
}

fn flood_fill(map: &mut Vec<Vec<char>>, x: usize, y: usize) {
    if map[x][y] != '.' {
        return
    }

    map[x][y] = 'O';
    if x < map.len() - 1 {
        flood_fill(map, x + 1, y);
    }
    if x > 0 {
        flood_fill(map, x - 1, y);
    }
    if y < map[0].len() - 1 {
        flood_fill(map, x, y + 1);
    }
    if y > 0 {
        flood_fill(map, x, y - 1);
    }
}

fn enlarge_map(map: &Vec<Vec<char>>) -> Vec<Vec<char>> {
    let mut res = Vec::new();

    for line in map {
        let mut new_line_top = Vec::new();
        let mut new_line = Vec::new();
        let mut new_line_bottom = Vec::new();
        for c in line {
            match c {
                '|' => {
                    new_line_top.push('.');
                    new_line_top.push('|');
                    new_line_top.push('.');
                    new_line.push('.');
                    new_line.push('|');
                    new_line.push('.');
                    new_line_bottom.push('.');
                    new_line_bottom.push('|');
                    new_line_bottom.push('.');
                },
                'L' => {
                    new_line_top.push('.');
                    new_line_top.push('|');
                    new_line_top.push('.');
                    new_line.push('.');
                    new_line.push('L');
                    new_line.push('-');
                    new_line_bottom.push('.');
                    new_line_bottom.push('.');
                    new_line_bottom.push('.');
                },
                'J' => {
                    new_line_top.push('.');
                    new_line_top.push('|');
                    new_line_top.push('.');
                    new_line.push('-');
                    new_line.push('J');
                    new_line.push('.');
                    new_line_bottom.push('.');
                    new_line_bottom.push('.');
                    new_line_bottom.push('.');
                },
                'F' => {
                    new_line_top.push('.');
                    new_line_top.push('.');
                    new_line_top.push('.');
                    new_line.push('.');
                    new_line.push('F');
                    new_line.push('-');
                    new_line_bottom.push('.');
                    new_line_bottom.push('|');
                    new_line_bottom.push('.');
                },
                '7' => {
                    new_line_top.push('.');
                    new_line_top.push('.');
                    new_line_top.push('.');
                    new_line.push('-');
                    new_line.push('7');
                    new_line.push('.');
                    new_line_bottom.push('.');
                    new_line_bottom.push('|');
                    new_line_bottom.push('.');
                },
                '-' => {
                    new_line_top.push('.');
                    new_line_top.push('.');
                    new_line_top.push('.');
                    new_line.push('-');
                    new_line.push('-');
                    new_line.push('-');
                    new_line_bottom.push('.');
                    new_line_bottom.push('.');
                    new_line_bottom.push('.');
                }
                _ => {
                    new_line_top.push('.');
                    new_line_top.push('.');
                    new_line_top.push('.');
                    new_line.push('.');
                    new_line.push('.');
                    new_line.push('.');
                    new_line_bottom.push('.');
                    new_line_bottom.push('.');
                    new_line_bottom.push('.');
                }
            }
        }
        res.push(new_line_top);
        res.push(new_line);
        res.push(new_line_bottom);
    }
    res
}

fn replace_start_(map: &mut Vec<Vec<char>>, start_x: usize, start_y: usize, orientation1: Orientation, orientation2: Orientation) {
    if orientation1 == NORTH && orientation2 == EAST {
        map[start_x][start_y] = 'L'
    } else if orientation1 == NORTH && orientation2 == SOUTH {
        map[start_x][start_y] = '|'
    } else if orientation1 == NORTH && orientation2 == WEST {
        map[start_x][start_y] = 'J'
    } else if orientation1 == EAST && orientation2 == SOUTH {
        map[start_x][start_y] = 'F'
    } else if orientation1 == EAST && orientation2 == WEST {
        map[start_x][start_y] = '-'
    } else if orientation1 == SOUTH && orientation2 == WEST {
        map[start_x][start_y] = '7'
    }
}

fn get_orientation(x: usize, y: usize, next_x: usize, next_y: usize) -> Orientation {
    if next_x > x {
        SOUTH
    } else if next_x < x {
        NORTH
    } else if next_y > y {
        EAST
    } else if next_y < y {
        WEST
    } else {
        NONE
    }
}

fn get_square_tiles(map: &Vec<Vec<char>>, x: usize, y: usize) -> Vec<char>
{
    let mut tiles = Vec::new();
    let map_height = map.len();
    let map_width = map[0].len();

    let start_x = x.saturating_sub(1);
    let end_x = (x + 1).min(map_height - 1);
    let start_y = y.saturating_sub(1);
    let end_y = (y + 1).min(map_width - 1);

    for i in start_x..=end_x {
        for j in start_y..=end_y {
            tiles.push(map[i][j]);
        }
    }
    tiles
}

fn replace_square_tiles(map: &mut Vec<Vec<char>>, x: usize, y: usize) {
    let map_height = map.len();
    let map_width = map[0].len();

    let start_x = x.saturating_sub(1);
    let end_x = (x + 1).min(map_height - 1);
    let start_y = y.saturating_sub(1);
    let end_y = (y + 1).min(map_width - 1);

    for i in start_x..=end_x {
        for j in start_y..=end_y {
            map[i][j] = ' ';
        }
    }
}


fn find_enclosed(map: &mut Vec<Vec<char>>) -> usize {
    let mut res = 0;
    for i in 1..map.len() - 1 {
        for j in 1..map[0].len() - 1 {
            if map[i][j] == '.' {
                let square = get_square_tiles(map, i, j);
                if square.iter().all(|c| *c == '.') {
                    // println!("Found {i}, {j}: {:?}", square);
                    res += 1;
                    replace_square_tiles(map, i, j);
                    // for c in square {
                    //     *c = ' ';
                    // }
                }
            }
            // if map[i][j] == '.' && map[i][j + 1]
        }
    }
    res
}

fn clean_map(map: &mut Vec<Vec<char>>, positions: &Vec<(usize, usize)>) {
    for i in 0..map.len() - 1 {
        for j in 0..map[0].len() - 1 {
            if map[i][j] != '.' && !positions.contains(&(i, j)) {
                // println!("Found useless pipe at {i}, {j}");
                map[i][j] = '.';
            }
        }
    }
}

fn display_map(map: &Vec<Vec<char>>) {
    for line in map {
        println!("{:?}", line.iter().collect::<String>());
    }
}

fn main() {
    let content = fs::read_to_string("inputs.txt").unwrap();
    // let content = fs::read_to_string("inputs_example.txt").unwrap();
    // let content = fs::read_to_string("inputs_example_part2.txt").unwrap();
    // let content = fs::read_to_string("example_sneak_walls.txt").unwrap();
    // let content = fs::read_to_string("example_part2_larger.txt").unwrap();
    // let content = fs::read_to_string("example_part2_larger_junk.txt").unwrap();

    let mut map: Vec<Vec<char>> = content.lines().map(|line| line.chars().collect()).collect();

    let start = find_starting_position(&map);
    let (x, y, prev) = find_next_pipe(&map, start.0, start.1, NONE);

    let mut res = get_path_positions(&map, x, y, prev);
    res.push((x, y));

    let mut part_1 = res.len();
    if part_1 % 2 == 0 {
        part_1 /= 2;
    } else {
        part_1 = part_1 / 2 + 1
    }
    println!("Result of part 1: {part_1}");

    let last_point = res.iter().nth(1).unwrap();
    let first_orientation = get_orientation(start.0, start.1, x, y);
    let second_orientation = get_orientation(start.0, start.1, last_point.0, last_point.1);

    replace_start_(&mut map, start.0, start.1, first_orientation, second_orientation);

    // display_map(&map);
    clean_map(&mut map, &res);
    // println!();
    // display_map(&map);
    let mut map = enlarge_map(&map);
    // display_map(&map);
    // for line in &map {
    //     println!("{:?}", line.iter().collect::<String>());
    // }
    // println!();
    // display_map(&map);
    flood_fill(&mut map, 0, 0);
    // for line in &map {
    //     println!("{:?}", line.iter().collect::<String>());
    // }
    let res = find_enclosed(&mut map);
    // display_map(&map);

    println!("Res for part 2: {res}");
}
