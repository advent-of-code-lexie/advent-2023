use std::fs;

fn get_empty_lines(map: &Vec<Vec<char>>) -> Vec<usize> {
    let mut res = Vec::new();

    for (index, line) in map.iter().enumerate() {
        if line.iter().all(|c| *c == '.') {
            res.push(index);
        }
    }

    res
}

fn get_empty_columns(map: &Vec<Vec<char>>) -> Vec<usize> {
    let mut res = Vec::new();

    for y in 0..map[0].len() {
        let mut galaxies = 0;
        for x in 0..map.len() {
            if map[x][y] == '#' {
                galaxies += 1
            }
        }
        if galaxies == 0 {
            res.push(y);
        }
    }

    res
}

fn get_galaxies_positions(map: &Vec<Vec<char>>) -> Vec<(usize, usize)> {
    let mut res = Vec::new();

    for x in 0..map.len() {
        for y in 0..map[0].len() {
            if map[x][y] == '#' {
                res.push((x, y));
            }
        }
    }

    res
}

fn calculate_sum_of_shortest_path(galaxies: &Vec<(usize, usize)>, rows: &Vec<usize>,
                                  columns: &Vec<usize>, multiplier: usize) -> usize {
    let mut result = 0;

    for i in 0..galaxies.len() {
        for j in i..galaxies.len() {
            let (x, y) = galaxies[i];
            let (x2, y2) = galaxies[j];

            let mut additional_steps = 0;
            for row in rows {
                if x < *row && *row < x2 {
                    additional_steps += 1;
                }
            }
            for column in columns {
                if (y < *column && *column < y2) || (y2 < *column && *column < y) {
                    additional_steps += 1;
                }
            }
            result += x.abs_diff(x2) + y.abs_diff(y2) + additional_steps * multiplier;
        }
    }

    result
}

fn main() {
    let content = fs::read_to_string("inputs.txt").unwrap();
    // let content = fs::read_to_string("inputs_example.txt").unwrap();

    let map: Vec<Vec<char>> = content.lines().map(|line| line.chars().collect()).collect();

    let rows = get_empty_lines(&map);
    let columns = get_empty_columns(&map);

    let positions = get_galaxies_positions(&map);

    let part_1 = calculate_sum_of_shortest_path(&positions, &rows, &columns, 2 - 1);
    println!("Result of part 1: {part_1}");
    let part_2 = calculate_sum_of_shortest_path(&positions, &rows, &columns, 1_000_000 - 1);
    println!("Result of part 2: {part_2}");
}
